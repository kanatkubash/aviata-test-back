<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 31.03.2019
 * Time: 22:16
 */

namespace App\Controllers;

use App\Services\CacheProviderInterface;
use App\Services\TimeProviderInterface;
use Carbon\Carbon;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class ApiController {
  /**
   * @var CacheProviderInterface
   */
  private $cacheProvider;
  /**
   * @var TimeProviderInterface
   */
  private $timeProvider;

  public function __construct(
    CacheProviderInterface $cacheProvider,
    TimeProviderInterface $timeProvider
  ) {
    $this->cacheProvider = $cacheProvider;
    $this->timeProvider = $timeProvider;
  }

  public function get(Request $request, Response $response) {
    $dir = $request->getQueryParams()["dir"];
    [$from, $to] = explode('-', $dir);
    $today = new Carbon($this->timeProvider->getCurrentTime());
    $endDate = $today->clone()->addMonth();
    $prices = [];

    while ($today <= $endDate) {
      $dateStr = $today->format('Y-m-d');
      $fare = $this->cacheProvider->get("$from-$to-$dateStr");
      $prices[$dateStr] = $fare ? json_decode($fare, true)['price'] : null;
      $today->addDay();
    }

    return $response->withJson($prices);
  }
}
