<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 01.04.2019
 * Time: 4:37
 */

namespace App\Services;


interface LowFareExtractInterface {
  public function getLowestPriceGroup($data);
}
