<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 31.03.2019
 * Time: 22:57
 */

namespace App\Services;

use Carbon\Carbon;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use function GuzzleHttp\json_decode;

class TestFlightsProvider implements FlightsProviderInterface {
  /**
   * @var ClientInterface
   */
  protected $httpClient;
  protected $statusEndpoint;
  protected $searchEndpoint;
  protected $maxAttempts;

  /**
   * TestFlightsProvider constructor.
   * @param ClientInterface $httpClient
   * @param array           $searchEndpoint
   * @param array           $statusEndpoint
   */
  public function __construct(
    ClientInterface $httpClient,
    array $searchEndpoint,
    array $statusEndpoint
  ) {
    $this->httpClient = $httpClient;
    $this->statusEndpoint = $statusEndpoint;
    $this->searchEndpoint = $searchEndpoint;
  }

  public function getFlights($from, $to, Carbon $date): array {
    ///TODO: should have used logger
    echo "Getting flights $from-$to on {$date->format('d.m.Y')}\r\n";
    $searchId = $this->createSearch($from, $to, $date);
    $flights = $this->fetchFlights($searchId);
    echo "Found flights $from-$to on {$date->format('d.m.Y')}\r\n";

    return $flights;
  }

  private function createSearch($from, $to, Carbon $date): string {
    $dateStr = $date->format('Ymd');
    $query = "$from-$to{$dateStr}1000E";
    ['url' => $url, 'token' => $token] = $this->searchEndpoint;

    $response = $this->sendRequest('POST', $url, $token, ['query' => $query]);

    return $response['id'];
  }

  private function fetchFlights($searchId): array {
    $status = null;
    $response = null;
    $attempts = 0;
    ['url' => $url, 'token' => $token] = $this->statusEndpoint;

    do {
      $response = $this->sendRequest('GET', "$url/$searchId", $token);
      usleep(500 * 1000);
      $status = $response['status'];
      $attempts++;
    } while ($status != 'done');

    return $response['items'] ?? [];
  }

  private function sendRequest($method, $url, $token, $body = []) {
    try {
      $options = [
        'headers' => ['Authorization' => "Bearer $token"],
      ];
      if ($method == 'POST') $options['json'] = $body;

      $response = $this->httpClient->request($method, $url, $options);

      return json_decode($response->getBody()->getContents(), true);
    } catch (RequestException $exception) {
      ///TODO: Log here
      throw  $exception;
    }
  }

}
