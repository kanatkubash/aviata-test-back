<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 31.03.2019
 * Time: 22:58
 */

namespace App\Services;

use Carbon\Carbon;

interface FlightsProviderInterface {
  public function getFlights($from, $to, Carbon $date): iterable;
}
