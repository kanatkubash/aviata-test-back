<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 01.04.2019
 * Time: 4:55
 */

namespace App\Services;

/**
 * This interface can be used to fake current date
 * Interface TimeProviderInterface
 * @package App\Services
 */
interface TimeProviderInterface {
  public function getCurrentTime(): \DateTime;
}
