<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 01.04.2019
 * Time: 8:23
 */

namespace App\Services;

use Carbon\Carbon;

/**
 * To check whether worker starts checking for new fares
 * Class TomorrowTimeProvider
 * @package App\Services
 */
class TomorrowTimeProvider implements TimeProviderInterface {
  public function getCurrentTime(): \DateTime {
    return Carbon::now()->addDay()->microsecond(0)->millisecond(0)->second(0)->hour(0)->minute(0);
  }
}
