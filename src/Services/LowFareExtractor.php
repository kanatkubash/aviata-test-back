<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 01.04.2019
 * Time: 4:37
 */

namespace App\Services;

class LowFareExtractor implements LowFareExtractInterface {
  public function getLowestPriceGroup($data) {
    $result = collect($data)
      ->reduce(
        function(array $carry, array $flightGroup) {
          ['price' => $minPrice] = $carry;
          ['price' => $amount, 'group' => $groupId] = $flightGroup;
          $price = intval($amount['amount']);

          if ($price < $minPrice)
            return ['group' => $groupId, 'price' => $price];
          else
            return $carry;
        },
        ['price' => PHP_INT_MAX, 'group' => null]
      );

    return $result['group'] ? $result : null;
  }
}
