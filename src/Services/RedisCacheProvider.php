<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 01.04.2019
 * Time: 4:50
 */

namespace App\Services;

use Predis\ClientInterface;

class RedisCacheProvider implements CacheProviderInterface, QueueCacheProviderInterface {
  /**
   * @var ClientInterface
   */
  private $client;
  /**
   * @var TimeProviderInterface
   */
  private $timeProvider;

  public function __construct(
    ClientInterface $client,
    TimeProviderInterface $timeProvider
  ) {
    $this->client = $client;
    $this->timeProvider = $timeProvider;
  }

  public function get(string $id): ?string {
    return $this->client->get($this->getId($id));
  }

  public function put(string $id, string $value, int $ttl = 0) {
    if ($ttl > 0)
      $this->client->setex(
        $this->getId($id),
        $value,
        $ttl
      );
    else
      $this->client->set(
        $this->getId($id),
        $value
      );
  }

  private function getCurDate(): string {
    return $this->timeProvider->getCurrentTime()->format('Y-m-d');
  }

  private function getId($id) {
    /// by adding curdate,
    /// we ensure that fares that retrieved on previous days are ignored
    $curDate = $this->getCurDate();

    return "$id-$curDate";
  }

  public function delete(string $id) {
    $this->client->del($id);
  }

  public function markProcessing(string $jobId) {
    /// if haven't processed within minute , delete the key
    $this->put("processing-$jobId", 1, 60);
  }

  public function markProcessed(string $jobId) {
    $this->delete("processing-$jobId");
    $this->delete("process-$jobId");
  }

  public function markToProcess(string $jobId) {
    /// in fact , I chose inappropriate queue worker,
    /// I should have implemented redis on sorted sets
    /// This way , I wouldn't have to keep track of jobs registered for process
    $this->put("process-$jobId", 1, 60);
  }

  public function isProcessing(string $jobId): bool {
    return $this->get("processing-$jobId") == "1";
  }

  public function isRegisteredToProcess(string $jobId): bool {
    return $this->get("process-$jobId") == "1";
  }
}
