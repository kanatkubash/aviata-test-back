<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 01.04.2019
 * Time: 4:57
 */

namespace App\Services;

use Carbon\Carbon;

/**
 * Provides real time
 * Class StandardTimeProvider
 * @package App\Services
 */
class StandardTimeProvider implements TimeProviderInterface {
  public function getCurrentTime(): \DateTime {
    /// for convinience when doing comparison operator against dates
    return Carbon::now()->microsecond(0)->millisecond(0)->second(0)->hour(0)->minute(0);
  }
}
