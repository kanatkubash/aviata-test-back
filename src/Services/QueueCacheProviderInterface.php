<?php
/**
 * Created by PhpStorm.
 * User: kanatkubash
 * Date: 01.04.2019
 * Time: 10:54
 */

namespace App\Services;


interface QueueCacheProviderInterface {
  public function markProcessing(string $jobId);

  public function markProcessed(string $jobId);

  public function markToProcess(string $jobId);

  public function isProcessing(string $jobId): bool;

  public function isRegisteredToProcess(string $jobId): bool;
}
