<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 01.04.2019
 * Time: 4:04
 */

namespace App\Services;

interface CacheProviderInterface {
  public function get(string $id);

  public function put(string $id, string $value, int $ttl = 0);

  public function delete(string $id);
}
