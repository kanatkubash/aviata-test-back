<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 01.04.2019
 * Time: 4:02
 */

namespace App\Queue;

use App\Services\CacheProvider;
use App\Services\CacheProviderInterface;
use App\Services\FlightsProviderInterface;
use App\Services\LowFareExtractInterface;
use App\Services\QueueCacheProviderInterface;
use App\Services\TimeProviderInterface;
use Carbon\Carbon;
use Simpleue\Job\Job;

class FareFindJob implements Job {
  /**
   * @var FlightsProviderInterface
   */
  private $flightsProvider;
  /**
   * @var QueueCacheProviderInterface
   */
  private $queueCacheProvider;
  /**
   * @var CacheProviderInterface
   */
  private $cacheProvider;
  /**
   * @var LowFareExtractInterface
   */
  private $lowFareExtract;
  /**
   * @var TimeProviderInterface
   */
  private $timeProvider;

  public function __construct(
    FlightsProviderInterface $flightsProvider,
    QueueCacheProviderInterface $queueCacheProvider,
    CacheProviderInterface $cacheProvider,
    LowFareExtractInterface $lowFareExtract,
    TimeProviderInterface $timeProvider
  ) {
    $this->flightsProvider = $flightsProvider;
    $this->queueCacheProvider = $queueCacheProvider;
    $this->cacheProvider = $cacheProvider;
    $this->lowFareExtract = $lowFareExtract;
    $this->timeProvider = $timeProvider;
  }

  public function manage($job) {
    $body = json_decode($job, true);
    [
      'from' => $from,
      'to'   => $to,
      'date' => $date,
    ] = $body;

    $flightId = "$from-$to-$date";

    /// in case we are sending repeated command
    if ($this->cacheProvider->get($flightId)) {
      $this->queueCacheProvider->markProcessed($flightId);

      return true;
    }

    try {
      /// to mark that current flight is being searched
      $this->queueCacheProvider->markProcessing($flightId);
      $flights = $this->flightsProvider->getFlights($from, $to, Carbon::parse($date));
      $lowestGroup = $this->lowFareExtract->getLowestPriceGroup($flights);
      $this->cacheProvider->put($flightId, json_encode($lowestGroup));
      $this->queueCacheProvider->markProcessed($flightId);

      return true;
    } catch (\Exception $e) {
      $this->queueCacheProvider->markProcessed($flightId);
      echo $e->getTraceAsString();

      return false;
    }
  }

  public function isStopJob($job) {
    ['createdDate' => $createdDate] = json_decode($job, true);

    // stop the job if it was created on previous day
    return Carbon::parse($createdDate) < $this->timeProvider->getCurrentTime();
  }

  public function isMyJob($job) {
    return true;
  }
}
