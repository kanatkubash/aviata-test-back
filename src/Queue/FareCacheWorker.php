<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 01.04.2019
 * Time: 5:09
 */

namespace App\Queue;

use App\Services\CacheProviderInterface;
use App\Services\QueueCacheProviderInterface;
use App\Services\TimeProviderInterface;
use Carbon\Carbon;
use Simpleue\Queue\Queue;
use Simpleue\Worker\QueueWorker;
use Symfony\Component\Process\Process;
use JakubOnderka\PhpConsoleColor\ConsoleColor;

class FareCacheWorker {
  private $client;

  /**
   * Workers are spawned using Symfony Process
   * @param $index
   * @return Process
   */
  public function spawn($index) {
    $process = new Process('php workerIndex.php slave');
    $console = new ConsoleColor();
    echo $console->apply("color_20", "Spawned $index-th instance\r\n");

    $process->start(function($type, $output) use ($console, $index) {
      echo $console->apply(
        "color_20",
        $index . " ##################################\r\n"
      );
      echo $output . "\r\n";
    });

    return $process;
  }

  /** Spawned workers run queue consumer
   * @param Queue       $queue
   * @param FareFindJob $job
   */
  public function work(Queue $queue, FareFindJob $job) {
    echo "Start working\r\n";
    $worker = new QueueWorker($queue, $job);
    $worker->start();
  }

  /** Master worker checks for cache
   * @param CacheProviderInterface      $cacheProvider
   * @param QueueCacheProviderInterface $queueCacheProvider
   * @param TimeProviderInterface       $timeProvider
   * @param Queue                       $queue
   * @param array                       $directions
   */
  public static function periodicalCheck(
    CacheProviderInterface $cacheProvider,
    QueueCacheProviderInterface $queueCacheProvider,
    TimeProviderInterface $timeProvider,
    Queue $queue,
    array $directions
  ) {
    $count = 0;
    collect($directions)
      ->map(
        function($dir) {
          return explode('-', $dir);
        }
      )
      ->each(
        function($direction) use ($count, $queueCacheProvider, $queue, $cacheProvider, $timeProvider) {
          [$from, $to] = $direction;
          $today = new Carbon($timeProvider->getCurrentTime());
          $todayStr = $today->format('Y-m-d');
          $endDate = $today->clone()->addMonth();

          while ($today <= $endDate) {
            $dateStr = $today->format('Y-m-d');
            $flightId = "$from-$to-$dateStr";
            if (!$cacheProvider->get($flightId) &&
                !$queueCacheProvider->isRegisteredToProcess($flightId) &&
                !$queueCacheProvider->isProcessing($flightId)) {
              $job = [
                'from'        => $from,
                'to'          => $to,
                'date'        => $dateStr,
                'createdDate' => $todayStr,
              ];
              $queueCacheProvider->markToProcess($flightId);
              $queue->sendJob(json_encode($job));
              $count++;
            }
            $today->addDay();
          }
        });
    if ($count != 0)
      echo "$count queues added\r\n";
    else
      echo "Clear\r\n";
  }
}
