##Usage

####Running daemon
`php workerIndex.php master`

This spawns 10 worker processes that search for fares. 
Master process takes care of adding fare search jobs to queue
After fares are found , they are persisted in Redis

Each fare date is stored in Redis with its fetched date, thus next day at 
00:00 because of date change Redis cache will be empty because dates are also
 used as keys

####Calendar view
Serve php using `php -S localhost:8888 route.php`

In browser, you should see a calendar and direction selector
When direction is selected, fare data within month is retrieved from backend

Reloading website will fill calendar with fare data as workers complete their
 jobs
