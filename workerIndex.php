<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 01.04.2019
 * Time: 5:11
 */

require "vendor/autoload.php";

use \Symfony\Component\Process\Process;
use \App\Queue\FareCacheWorker;
use \App\Services\{
  CacheProviderInterface,
  TimeProviderInterface,
  QueueCacheProviderInterface
};
use Simpleue\Queue\RedisQueue;
use Predis\ClientInterface;

[$_, $kind] = $argv;
$count = 10;

$container = (new \DI\ContainerBuilder)
  ->addDefinitions(__DIR__ . '/config.php')
  ->build();

$queue = new RedisQueue(
  $container->get(ClientInterface::class),
  'cache-queue'
);

if ($kind == 'master') {
  $processes = collect([]);
  for ($i = 0; $i < $count; $i++) {
    $worker = new FareCacheWorker();
    $processes->push($worker->spawn($i));
  }
  while (true) {
    $processes->each(function(Process $process) {
      $process->getIncrementalOutput();
    });
    FareCacheWorker::periodicalCheck(
      $container->get(CacheProviderInterface::class),
      $container->get(QueueCacheProviderInterface::class),
      $container->get(TimeProviderInterface::class),
      $queue,
      $container->get('directions')
    );
    sleep(1);
  }
} elseif ($kind == 'slave') {
  $worker = new FareCacheWorker();
  $worker->work($queue, $container->get(\App\Queue\FareFindJob::class));
}
