<?php
return [
  'propel' => [
    'database' => [
      'connections' => [
        'default' => [
          'adapter'  => 'sqlite',
          'dsn'      => 'sqlite:' . __DIR__ . PATH_SEPARATOR . 'my.app.sq3',
          'user'     => 'test',
          'password' => 'test',
          'settings' => [
            'charset' => 'utf8'
          ]
        ]
      ]
    ]
  ]
];
