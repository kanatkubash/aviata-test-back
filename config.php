<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 31.03.2019
 * Time: 23:23
 */

use App\Controllers\ApiController;
use function \DI\{
  get, autowire
};

/**
 * We could get this information from env file
 */
$token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJhZGE1ZWVkYi1mMDg3LTQzMjQtYmY3Ny0yZjcxMWNkOTY1M2UiLCJpc3MiOiJZdmQ1TFNVbUM3MHVWQnJxNmFFTlFoNjBIQm4waUN6WiIsImlhdCI6MTU0NzgwODE4MCwiZXhwIjoyODcyMzIwMTgwLCJjb25zdW1lciI6eyJpZCI6IjFhMGQ3ZGMyLTZlN2YtNDE5Mi1hY2Y3LTU0NGYyNzQzMzdmYSIsIm5hbWUiOiJjaG9jby1rei5zaXRlIn19.eEM266jdJ2Cc8IEDDkC9lDJbkc3_YBwLpLEkqshcjRg';
$searchEndpoint = [
  'url'   => 'https://kong-staging-platform.dev.cloud.aviata.team/airflow/search',
  'token' => $token,
];
$statusEndpoint = [
  'url'   => 'https://kong-staging-platform.dev.cloud.aviata.team/airflow/search/results',
  'token' => $token,
];


return [
  'settings.displayErrorDetails'             => true,

  /// by altering this we can today date to whatever date to simulate for example tomorrow
  \App\Services\TimeProviderInterface::class => get(\App\Services\StandardTimeProvider::class),

  \App\Services\FlightsProviderInterface::class    => autowire(\App\Services\TestFlightsProvider::class)
    ->constructorParameter('searchEndpoint', $searchEndpoint)
    ->constructorParameter('statusEndpoint', $statusEndpoint),
  \App\Services\CacheProviderInterface::class      => get(\App\Services\RedisCacheProvider::class),
  \App\Services\QueueCacheProviderInterface::class => get(\App\Services\RedisCacheProvider::class),
  \App\Services\LowFareExtractInterface::class     => get(\App\Services\LowFareExtractor::class),
  \GuzzleHttp\ClientInterface::class               => get(GuzzleHttp\Client::class),
  \Predis\ClientInterface::class                   => function() {
    $client = new Predis\Client();
    ///not to interfere with my main db
    $client->select(1);

    return $client;
  },

  'directions' => [
    'ALA-TSE',
    'TSE-ALA',
    'ALA-MOW',
    'MOW-ALA',
    'ALA-CIT',
    'CIT-ALA',
    'TSE-MOW',
    'MOW-TSE',
    'TSE-LED',
    'LED-TSE',
  ]
];
