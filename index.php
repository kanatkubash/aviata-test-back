<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 31.03.2019
 * Time: 22:02
 */

require "vendor/autoload.php";

use DI\Bridge\Slim\App;
use \App\Controllers\ApiController;

$app = new class() extends App {
  protected function configureContainer(\DI\ContainerBuilder $builder) {
    $builder->addDefinitions(__DIR__ . '/config.php');
    parent::configureContainer($builder);
  }
};

$app->get('/api', [ApiController::class, 'get']);

$app->run();
