<?php
/**
 * Created by PhpStorm.
 * User: kanat
 * Date: 01.04.2019
 * Time: 7:16
 */

class LowFareExtractorTest extends PHPUnit\Framework\TestCase {
  public function testLowFareIsCorrect() {
    $strData = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'response.json');
    $data = json_decode($strData, true);
    $extractor = new \App\Services\LowFareExtractor();

    $priceData = $extractor->getLowestPriceGroup($data['items']);

    $this->assertEquals(
      '53b5a8295049d63decf8436c7549601d',
      $priceData['group']
    );

    $this->assertEqualsCanonicalizing(
      ['price' => 21591, 'group' => '53b5a8295049d63decf8436c7549601d'],
      $priceData
    );
  }
}
